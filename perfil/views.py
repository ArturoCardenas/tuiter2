from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from perfil.models import Tuit
import datetime

@login_required
def home(request):
	context = RequestContext(request, {
		'tuits': request.user.tuits.all()
	})
	return render_to_response('perfil.html', context)

@login_required
def tuitear(request):
	if request.method == 'POST':
		tuit = Tuit()
		tuit.mensaje = request.POST.get('tuit')
		tuit.usuario = request.user
		tuit.fecha = datetime.datetime.now()
		tuit.save()
	return HttpResponseRedirect('/perfil/')


